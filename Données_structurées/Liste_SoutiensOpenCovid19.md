Soutiens mémo #opencovid19
### Comptes Twitter

@ADOA_solutions
@ADULLACT
@apifrorg
@caresteouvert
@cnll_fr
@CodeForFR
@framasoft
@HackResearch
@Inno_3
@CoopdesCommuns
@fab_mob
@La_Mouette__
@laMYNE_
@Montpellibre
@naos_cluster
@opendata_fr
@OSM_FR
@Wikimedia_Fr
@Coexiscience
@worteks_com
@Federation_OSM

### Pas de compte Twitter

covid-initiatives.org
vvlibri.org
Cap sur les Communs
LibreAccès
Faire Ecole Ensemble (Fée)
Sharelex

### Pas d'URL vers le mémo

ADOA
ADULLACT
Ça reste ouvert
Cap sur les Communs
Covid-initiatives.org
La Fabrique des Mobilités
LibreAccès
DRISS


### Mots clé et hashtags à intégrer à la requête

opencovid19
open
conscience
mémorandum
covid19
libre
impulsions
OPENCOVID
COVIDー19
COVID19fr


### URLs du mémo sur le site interne + les sites des soutiens

https://covid19-open.frama.io/memo/

https://cnll.fr/news/le-cnll-soutien-le-m%C3%A9morandum-covid-19-pour-du-libre-et-de-lopen-en-conscience/

https://montpellibre.fr/spip.php?article4807

https://inno3.fr/actualite/memorandum-covid-19-pour-du-libre-et-de-lopen-en-conscience-enseignements-et-impulsions

https://naos-cluster.com/2020/04/29/lettre-ouverte-pour-du-libre-et-de-lopen-en-conscience/

http://www.opendatafrance.net/2020/04/30/covid-19-pour-du-libre-et-de-lopen-en-conscience/

https://www.openstreetmap.fr/memorandum-pour-du-libre-et-de-lopen-en-conscience/

https://vvlibri.org/fr/blog/memorandum2020

https://www.wikimedia.fr/covid-19-pour-du-libre-et-de-lopen-en-conscience/

https://codefor.fr/assemblies/codeforfr/f/47/posts/16

https://framablog.org/2020/04/29/memorandum-covid-19-pour-du-libre-et-de-lopen-en-conscience-enseignements-et-impulsions-futures/

https://hyr.science/2020/04/memorandum-covid-19-pour-du-libre-et-de-lopen-en-conscience-enseignements-et-impulsions-futures/

https://coopdescommuns.org/fr/soutien-covid-19-pour-du-libre-et-de-lopen-en-conscience/

https://www.lamouette.org/index.php/communiques/90-covid-19-pour-du-libre-et-de-l-open-en-conscience

https://pad.lamyne.org/s/memorandum-pour-du-libre-et-de-l-open-en-conscience#

http://www.coexiscience.fr/coexiscience-introduisant-ce-memorandum-covid-19-pour-du-libre-et-de-lopen-en-conscience/

https://sharelex.org/t/memorandum-covid-19-pour-du-libre-et-de-l-open-en-conscience-enseignements-et-impulsions-futures/938

https://www.worteks.com/fr/2020/05/18/covid-19-pour-du-libre-et-de-lopen-en-conscience/

https://wiki.faire-ecole.org/wiki/M%C3%A9morandum_Covid-19_pour_du_libre_et_de_l%27open_en_conscience_:_enseignement_et_impulsions_futures#Mesures_actionnables

https://www.federation-openspacemakers.com/fr/communautes/actualites/covid-19-pour-du-libre-et-de-lopen-en-conscience-enseignements-et-impulsions-futures/

