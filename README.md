# Description

Ce projet est dédié au stockage et à l'analyse de données collectées sur la diffusion du [Mémorandum Covid-19 - Pour du libre et de l'open en conscience](https://covid19-open.frama.io/memo/) via Twitter du 29 avril au 27 mai 2020.

# Contenu des fichiers

## Données brutes:

Fichier contenant les données collectées de Twitter et leur description

### Collecte1_29avril-6mai2020

### Collecte2_7-13mai2020

### Collecte3_14-20mai2020

### Collecte4_21-27mai2020

## Données structurées

Fichiers contenant les données structurées en format jsonl et csv

`Liste_SoutiensOpenCovid19.md`

- la liste des comptes Twitter des soutiens du mémo
- la liste des url vers les sites des soutiens qui ont publié le mémo
- la liste des soutiens qui n'ont pas publié le mémo sur leur site
- la liste des soutiens sans comptes Twitter

`Protocole collecte tweets.md`

- la liste des mots clefs et hashtags utilisés pour les requêtes
- des exemples détaillés des requêtes utilisées

## Code

Notebook Jupyter contenant le code utilisé pour créer un réseau de retweets

## Viz

Visualisations faites avec Gephi et [Twitter Explorer](https://twitterexplorer.org/)

## Analyses préliminaires dans le wiki
