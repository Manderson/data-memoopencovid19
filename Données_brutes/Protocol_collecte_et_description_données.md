# Protocole collecte tweets mémo #opencovid19

## Exemples de requête Twarc

**Succession itérative de requêtes en partant de la plus vaste pour aller vers la plus précise**

*Requête 1*

- pour avoir un aperçu des tweets associés au sujet de l'open dans le context du covid-19, cette requête a été faite pour chacun des soutiens du mémorandum. Dans l'exemple ci-dessous, on peut rajouter autant de noms d'utilisateur de soutiens que l'on veut séparés par 'OR'.

```twarc search 'opencovid19 OR open OR conscience OR mémorandum OR covid-19 OR covid19 OR libre OR opencovid OR covid19fr OR impulsions @ADOA_solutions OR @ADULLACT OR @apifrorg OR @caresteouvert OR @cnll_fr OR @CodeForFR OR @framasoft OR @HackResearch OR @Inno_3 OR @CoopdesCommuns OR @fab_mob OR @La_Mouette__ OR @laMYNE_ OR @Montpellibre OR @naos_cluster OR @opendata_fr OR @OSM_FR OR @Wikimedia_Fr OR @Coexiscience since:2020-05-13' > all_opencovid_tweets.jsonl```


*Requête 2*

- pour avoir tous les tweets et retweets contenant la chaîne de caractère "pour du libre et de l’open en conscience" et le hashtage #open, tels qu'ils apparaissent dans [le tweet du 29 avril 2020 d'@Inno_3](https://twitter.com/Inno_3/status/1255447968246902785).

```twarc search '("pour du libre et de l’open en conscience" AND Covid-19) (#open)' > memo_opencovid_retweets1.jsonl ```

*Requête 3*

- pour avoir tous les tweets et retweets contenant l'url vers la page d'un site d'un soutien ayant publié le mémorandum. Dans l'exemple ci-dessous, on peut remplacer l'URL entre "" par l'url vers la page d'un site d'un soutien (bien conserver les "" pour la requête).

```twarc search "https://covid19-open.frama.io/memo/" > url_covid19-open_tweets.jsonl```

____________________________________________
# Description des fichiers
____________________________________________
### Dossier Collecte1_29avril-6mai2020



| Nom du fichier | Nature du contenu | 
| -------- | -------- | -------- |
| memo_opencovid_retweets1.jsonl     | requête 2 (tweets + retweets mémo)


| Nom du dossier | Nature du contenu | Fichiers sans données |
| -------- | -------- | -------- |
| fromuser_opencovid_twarc_data     | requête 1 (fractionnée)     | from_ADOAsolutions_opencovid_tweets.jsonl ; from_CoopdesCommuns_opencovid_tweets.jsonl       |
| hashtags_opencovid_twarc_data     | requête 1 (fractionnée)     | ADOA_solutions_opencovid_tweets.jsonl  |
| url_opencovid_twarc_data   |requête 3  (tweets + retweets URL soutien)   | url_cnll_tweets.jsonl ; url_coopdescommuns_tweets.jsonl ; url_hyr_tweets.jsonl ; url_montpellibre_tweets.jsonl ; url_openstreetmap_tweets.jsonl ;  url_vvlibri_tweets.jsonl

### Collecte2_7-13mai2020

[optimisation de la requête 1]

| Nom du fichier | Nature du contenu | 
| -------- | -------- | -------- |
| memo_opencovid_retweets2.jsonl   | requête 2 


| Nom du dossier | Nature du contenu | Fichiers sans données |
| -------- | -------- | -------- |
|   username_hashtags_opencovid_tweets   | requête 1 (optimisée)    | ADOA_opencovid_tweets.jsonl ; apifrorg_opencovid_tweets.jsonl ; cnll_fr_opencovid_tweets.jsonl ; CodeForFR_opencovid_tweets.jsonl ; fab_mob_opencovid_tweets.jsonl ; HackResearch_opencovid_tweets.jsonl ; La_Mouette_opencovid_tweets.jsonl ; laMYNE_opencovid_tweets.jsonl ; naos_cluster_opencovid_tweets.jsonl ;     |
|  url_opencovid_tweets   | requête 3     | url_cnll_tweets.jsonl ; url_coexiscience_tweets.jsonl ; url_coopdescommuns_tweets.jsonl ;  url_hyr_tweets.jsonl ; url_inno3_tweets.jsonl ; url_montpellibre_tweets.jsonl ; url_naos_tweets.jsonl ; url_openstreetmap_tweets.jsonl ; url_vvlibri_tweets.jsonl |


### Collecte3_14-20mai2020


| Nom du fichier | Nature du contenu | 
| -------- | -------- | -------- |
|   hashtags_allusernames_opencovid_tweets.jsonl  | requête 1 (optimisée)


| Nom du dossier | Nature du contenu | Fichiers sans données |
| -------- | -------- | -------- |
|      | Text     | Text     |
|     | Text     | Text     |
|    | Text     | Text     |
|      | Text     | Text     |
|      | Text     | Text     |



### Collecte4_21-27mai2020

| Nom du fichier | Nature du contenu | 
| -------- | -------- | -------- |
|  memo_opencovid_retweets4.jsonl   | requête 2 
|  all_opencovid_tweets_collecte4.jsonl  | requête 1 (optimisée) 


| Nom du dossier | Nature du contenu | Fichiers sans données |
| -------- | -------- | -------- |
|      | Text     | Text     |
|     | Text     | Text     |
|    | Text     | Text     |
|      | Text     | Text     |
|      | Text     | Text     |

____________________________________________
